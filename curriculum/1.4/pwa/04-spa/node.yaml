name: Single-Page Applications
description: Implement SPA in vanilla JavaScript.
days: 2
goals:
    javascript: 1
    dom: 1
    spa: 1
assignment:
    Introduction: |
        The goal is to create a single page web application *without the use of any client-side JavaScript framework/libraries*. The app is a To-Do list manager. User login is not required - any user with access to the service can view and modify any item. The REST API is already implemented in `api.js` -- you should not need to change it.

        To get an idea what to aim for, watch the [demo video](https://video.saxion.nl/media/t/1_7u12hugl). (No audio.)

        Be advised that creating an app like this without the use of framework can get a little messy. Of course you should try your best to keep your code as tidy as you can, but we teacher promise to be a bit more lenient than usual in our code review.

    REST API documentation: |
        ## Get all lists
        `GET /api/lists`

        Status code 200. Response body example:
        ```js
        [
            {id: 1, name: "Groceries"},
            {id: 2, name: "Apollo Project"}
        ]
        ```

        ## Create a new list
        `POST /api/lists`

        Returns the newly created item including the id that was assigned to it.

        Request body example:
        ```js
        {name: "WebTech"}
        ```
        Status code 201. Response body example:
        ```js
        {id: 3, name: "WebTech"}
        ```
        Status code 400. Response body example:
        ```js
        {error: "Invalid name"}
        ```

        ## Delete a list
        `DELETE /api/lists/:listId`
        
        Status code 200. Response body example:
        ```js
        {}
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such list"}
        ```

        ## Get all items for a list
        `GET /api/lists/:listId/items`

        Status code 200. Response body example:
        ```js
        [
            {id: 1, name: "Sugar", checked: false},
            {id: 2: name: "Milk", checked: true}
        ]
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such list"}
        ```
        

        ## Create a new item for a list
        `POST /api/lists/:listId/items`

         Returning the newly created item including its assigned id.

        Request body example:
        ```js
        {name: "Build a spaceship"}
        ```
        Status code 201. Response body example:
        ```js
        {id: 1, name: "Build a spaceship", checked: false}
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such list"}
        ```
        Status code 400. Response body example:
        ```js
        {error: "Invalid name"}
        ```

        
        ## Delete an item
        `DELETE /api/lists/:listId/items/:itemId`
        
        Status code 200. Response body example:
        ```js
        {}
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such item"}
        ```


        ## Change properties of an item
        `PUT /api/lists/:listId/items/:itemId`

         Returns the new state of the item.

        Request body example:
        ```js
        {checked: true, name: "Build a HUGE spaceship"}
        ```
        Any combination of `checked` and `name` can be specified. Unspecified properties will be kept at their current value.

        Status code 200. Response body example:
        ```js
        {id: 1, name: "Build a HUGE spaceship", checked: true}
        ```
        Status code 404. Response body example:
        ```js
        {error: "No such item"}
        ```


    Non-functional requirements:
        - |
            We recommend that you first carefully study these non-functional requirements, and then start working on the functional requirements. When you're done with the functional requirement, you can go over the non-functionals once more, to make sure that your work ticks all the boxes.

        -
            must: true
            text: You are *not* to use any client-side JavaScript **libraries**, and you must *not* edit any `.html` files. Instead, you should directly use the DOM API to create the user interface, using methods/properties such a `document.createElement`, `innerText`, `className`, `setAttribute`, `appendChild`, `addEventListener`, etc. `innerHTML` may *not* be used.
        -
            must: true
            text: The user-interface should be (roughly) the same as in the demo video. To that end, you can use the provided `style.css` file, and have a peek in the `static-html-examples` directory.
        -
            ^merge: feature
            code: 0
            weight: 10
            text: |
                Implement and use a helper function (or functions) to prevent too much repetitiveness due to these DOM calls. Give this some thought - what would be the best way to keep your code short and readable?
        -
            ^merge: feature
            code: 0
            weight: 25
            text: |
                All changes (except switching tabs) should persist to the database using the provided REST API. When reloading the site (on a different device) the state should be restored.
        -
            must: true
            text: Use the browser `fetch` API for doing HTTP requests from JavaScript.
        -
            ^merge: feature
            code: 0
            weight: 5
            text: |
                Implement a helper function around `fetch` for working with the REST API, and use it to prevent code duplication.
        -
            ^merge: feature
            code: 0
            weight: 5
            text: |
                All changes (except maybe item and list creation) should be reflected in the user-interface immediately, without waiting for a round-trip to the server. To help diagnose this, the provided REST API adds a 1s delay to all operations that should not require a round-trip before the UI is updated.

                You may for now assume that there will be no errors while performing the REST request.
        -
            ^merge: feature
            code: 0
            weight: 5
            text: |
                For icons, use Google's [Material Design Icons](https://material.io/tools/icons/). The [icon font stylesheet](https://google.github.io/material-design-icons/#icon-font-for-the-web) has already been included from the `index.html`.

    Functional requirements:
        -
            ^merge: feature
            code: 0
            weight: 10
            text: |
                The application should contain multiple lists of items. Each list will be represented by a tab in the UI.
        -
            ^merge: feature
            code: 0
            weight: 10
            text: |
                The left-most tab should be special. Clicking it will present an interface for creating a new list. The tab shouldn't have a title but a suitable icon instead.
                
                All other tabs show a list of To-Do items.
        -
            ^merge: feature
            code: 0
            weight: 10
            text: |
                There should be an 'add item' button (or icon). Clicking it will open a modal dialog, containing:

                - A text field for the To-Do item name.
                - A cancel button that just closes the modal.
                - A save button that persists the changes and closes the modal.
        -
            ^merge: feature
            code: 0
            weight: 5
            text: |
                There's a delete icon. Clicking it will delete the entire list (including the tab) and switch to the 'new list' tab.
        -
            ^merge: feature
            code: 0
            weight: 5
            text: |
                Each item shows a checkmark, the name and an suitable edit icon.
        -
            ^merge: feature
            code: 0
            weight: 5
            text: |
                Clicking the edit icon should open a modal very similar to (and sharing code with) the 'add item' modal. It has one extra button containing a trash icon, for removing the item.
        -
            ^merge: feature
            code: 0
            weight: 5
            text: |
                Clicking anywhere else toggles the 'done' status of an item. Items marked as 'done' have their check marks checked.
        - |
            Don't forget to check the non-functional requirements!
    
    Non-requirements: |
        - You are not required to handle errors caused by multiple users/browsers interacting with your server simultaneously.
        - Connection errors don't need to be handled either.
        - Unexpected server responses (such as errors) don't need to be handled.
        - The demo video maintains the selected tab on page reload. You don't need to do that. (But if you want to, search for `localStorage`.)
        - Your app does not need to be cross-browser compatible. Just a recent Chrome or Firefox is enough.
