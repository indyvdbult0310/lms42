from lightlib import *
from unittest import TestCase
test = TestCase()

# Create the hub
hub = Hub()

# Create a room
kitchen = Room("kitchen")
hub.add_room(kitchen)
test.assertEqual(kitchen.get_name(), "kitchen")
test.assertEqual(str(kitchen), "kitchen {\n}")
assert hub.get_room("kitchen") is kitchen
test.assertEqual(hub.get_room("no-such-room"), None)

# Create a light
kitchen_ceiling = Light("ceiling")
test.assertEqual(kitchen_ceiling.get_name(), "ceiling")
test.assertEqual(str(kitchen_ceiling), "ceiling → l0")

# Add a light to a room
kitchen.add_light(kitchen_ceiling)
assert kitchen.get_light("ceiling") is kitchen_ceiling
test.assertEqual(kitchen.get_light("no-such-light"), None)
test.assertEqual(str(kitchen), """kitchen {
  ceiling → l0
}""")

# Set the light to maximum brightness
kitchen_ceiling.set_state(LightState(42))
test.assertEqual(str(kitchen), """kitchen {
  ceiling → l42
}""")

# The bedroom has three lights, including a color light
hub.add_room(Room("bedroom"))
hub.get_room("bedroom").add_light(Light("bedside left"))
hub.get_room("bedroom").add_light(Light("bedside right"))
hub.get_room("bedroom").add_light(ColorLight("closet"))
test.assertEqual(str(hub.get_room("bedroom")), """bedroom {
  bedside left → l0
  bedside right → l0
  closet → l0 h0 s0
}""")

# Set romantic bedroom atmosphere
bedroom = hub.get_room("bedroom")
deep_red = ColorLightState(30, 324, 100)
bedroom.set_state(deep_red)
test.assertEqual(str(bedroom), """bedroom {
  bedside left → l30
  bedside right → l30
  closet → l30 h324 s100
}""")

# Some extra reading light on one side of the bed
bedroom.get_light("bedside right").set_state(LightState(50))
test.assertEqual(str(bedroom), """bedroom {
  bedside left → l30
  bedside right → l50
  closet → l30 h324 s100
}""")

# Save this as a scene
bedroom.store_scene('romantic reading')

# Set to full brightness, and save as another scene
bedroom.set_state(LightState(100))
test.assertEqual(str(bedroom), """bedroom {
  bedside left → l100
  bedside right → l100
  closet → l100 h0 s0
}""")
bedroom.store_scene('bright')

# Restore a scene
bedroom.recall_scene('romantic reading')
test.assertEqual(str(bedroom), """bedroom {
  bedside left → l30
  bedside right → l50
  closet → l30 h324 s100
}""")
bedroom.recall_scene('bright')
test.assertEqual(str(bedroom), """bedroom {
  bedside left → l100
  bedside right → l100
  closet → l100 h0 s0
}""")

# Check complete Hub
test.assertEqual(str(hub), """Hub>>>
kitchen {
  ceiling → l42
}
bedroom {
  bedside left → l100
  bedside right → l100
  closet → l100 h0 s0
}
<<<Hub""")
print("All tests passed")