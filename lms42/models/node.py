from ..app import db
import sqlalchemy as sa

class Node(db.Model):
    __tablename__ = "nodes"
    id = db.Column(db.Text, primary_key=True)
    info = db.Column(sa.dialects.postgresql.JSON, nullable=False)
