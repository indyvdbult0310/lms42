from ..app import db, app
from ..working_days import get_current_octant, get_octant_dates
from ..models.user import User, TimeLog
from ..models.attempt import Attempt
from ..routes.inbox import get_students
from ..models import curriculum
import sqlalchemy as sa
import flask
import datetime

@app.route('/tv', methods=['GET'])
@app.route('/tv/<device>', methods=['GET'])
def tv(device=None):
    fragment = flask.request.args.get('fragment')
    template = fragment if fragment in ('queue',) else 'tv'

    if device in ('a','b',):
        class_filter = f"ESD1V.{device}"
    elif device == 'd':
        class_filter = f"DSD1V.a"
    else:
        class_filter = None

    students = get_students(class_filter=class_filter)
    return flask.render_template(template+'.html',
        header=False,
        device=device,
        students=students,
    )
